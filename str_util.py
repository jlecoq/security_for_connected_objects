def equal(str1: str, str2: str) -> bool:
    str1_size = len(str1)

    if str1_size != len(str2):
        return False

    for i in range(str1_size):
        if str1[i] != str2[i]:
            return False

    return True