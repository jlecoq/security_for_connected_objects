from pseudo_random_number_generator import PseudoRandomNumberGenerator
from math_util import is_prime, gcd, prime_factors, is_divisible

# encrypte value of the sensor of the pyboard (accelerometer for example)
# Init seed with the temperature of the pyboard.

# Formumla: Xn+1 = (a*Xn + c) % m
class LinearCongruentialGenerator(PseudoRandomNumberGenerator):
    
    def __init__(self):
        self.seed = 0
        self.a = 22_695_477
        self.c = 1
        self.m = 2**32
        self.max = self.m - 1 # Not used right now, but could be used later, for example to generate number betwee [0, 1).
    
    def set_parameters(self, seed, a, c, m):
        self.seed = seed
        self.a = a
        self.c = c
        self.m = m

    def rand_int(self): 
        self.seed = (self.a * self.seed + self.c) % self.m 

        # We remove the 16 lower-order bits (right shift) because they have a shorter periods than the higher-order bits, they are less "random".
        return self.seed >> 16 

    def rand_ints_without_shifting(self, number_of_generations):
        values = []

        for i in range(number_of_generations):
            self.seed = (self.a * self.seed + self.c) % self.m
            values.append(self.seed)

        return values

    def are_good_parameters(self, seed, a, c, m):
        condition1_valid = condition2_valid = condition3_valid = True

        result = gcd(c, m)

        if result != 1:
            condition1_valid = False

        p_factors = prime_factors(m)

        for p_factor in p_factors:
            condition2_valid = is_divisible((a - 1), p_factor)

        if is_divisible(m, 4):
            condition3_valid = is_divisible((a - 1), 4)

        return condition1_valid and condition2_valid and condition3_valid


        