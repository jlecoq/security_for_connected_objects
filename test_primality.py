from math_util import is_probably_prime_fermat, prime_numbers, is_probably_prime_miller_rabin
from sympy.ntheory import isprime
from plt_util import display_bar_histogram
from print_colors import OKGREEN, ENDC

def test_primality_fermat_miller_rabin_sympy():
    print(f'{OKGREEN}RUN TEST: primality test Fermat, Miller-Rabin, Sympy:{ENDC}\n')

    n = 1_000_000

    # We generates every prime numbers below n
    p_numbers = prime_numbers(n)
    count_of_primes_number = len(p_numbers)

    detected_as_primes = [0, 0, 0]

    for p_number in p_numbers:
        if is_probably_prime_fermat(p_number):
            detected_as_primes[0] += 1

        if is_probably_prime_miller_rabin(p_number):
            detected_as_primes[1] += 1

        if isprime(p_number):
            detected_as_primes[2] += 1

    labels = ('Fermat', 'Miller-Rabin', 'Sympy')

    options = {
        'ylabel': 'Quantites',
        'title': f'Number of detected prime numbers on {count_of_primes_number} prime numbers.'
    }

    display_bar_histogram(detected_as_primes, labels, options)

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')
