from diffie_hellman import diffie_hellman
from print_colors import OKGREEN, ENDC

def test_diffie_hellman():
    print(f'{OKGREEN}RUN TEST: Diffie-Hellman:\n{ENDC}')

    diffie_hellman()

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')
