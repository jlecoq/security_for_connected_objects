from pseudo_random_number_generator import PseudoRandomNumberGenerator

class Isaac(PseudoRandomNumberGenerator):
    def __init__(self, seed_vector=[0] * 256):
        self.mm = [0] * 256
        self.randrsl = seed_vector
        self.randcnt = 0
        self.aa = 0
        self.bb = 0
        self.cc = 0
        self.mod = 2 ** 32

        self.__randinit__(True)

    def mix(self, a, b, c, d, e, f, g, h):
        a ^= 0xFFFFFFFF & b << 11
        d = (d + a) % self.mod
        b = (b + c) % self.mod
        
        b ^= 0x3FFFFFFF & (c >> 2)
        e = (e + b) % self.mod
        c = (c + d) % self.mod
        
        c ^= 0xFFFFFFFF & d << 8
        f = (f + c) % self.mod
        d = (d + e) % self.mod
        d ^= e >> 16
        g = (g + d) % self.mod
        e = (e + f) % self.mod
        
        e ^= 0xFFFFFFFF & f << 10
        h = (h + e) % self.mod
        f = (f + g) % self.mod
        
        f ^= 0x0FFFFFFF & (g >> 4)
        a = (a + f) % self.mod
        g = (g + h) % self.mod
        
        g ^= 0xFFFFFFFF & h << 8
        b = (b + g) % self.mod
        h = (h + a) % self.mod
        
        h ^= 0x007FFFFF & (a >> 9)
        c = (c + h) % self.mod
        a = (a + b) % self.mod
        
        return a, b, c, d, e, f, g, h

    def rand_int(self, mod=2 ** 32):
        if self.randcnt == 256:
            self.__isaac__()
            self.randcnt = 0

        res = self.randrsl[self.randcnt] % mod
        self.randcnt += 1

        return res

    def rand_ints(self, number_of_generations: int):
        values = []

        for i in range(number_of_generations):
            values.append(self.rand_int())

        return values

    def rand_int_of(self, number_of_digits: int):
        number_str = str(self.rand_int())
        
        while len(number_str) < number_of_digits:
            number_str += str(self.rand_int())

        if len(number_str) > number_of_digits:
            number_str = number_str[:number_of_digits]

        return int(number_str)

    def __isaac__(self):
        self.cc += 1
        self.bb += self.cc
        self.bb &= 0xFFFFFFFF

        for i in range(256):
            x = self.mm[i]
            switch = i % 4
            xorwith = None

            if switch == 0:
                xorwith = (self.aa << 13) % self.mod
            elif switch == 1:
                xorwith = self.aa >> 6
            elif switch == 2:
                xorwith = (self.aa << 2) % self.mod
            elif switch == 3:
                xorwith = self.aa >> 16
            else:
                raise Exception("math is broken")
            
            self.aa = self.aa ^ xorwith
            self.aa = (self.mm[(i + 128) % 256] + self.aa) % self.mod
            y = self.mm[i] = (self.mm[(x >> 2) % 256] + self.aa + self.bb) % self.mod
            self.randrsl[i] = self.bb = (self.mm[(y >> 10) % 256] + x) % self.mod

    def __randinit__(self, flag):
        a = b = c = d = e = f = g = h = 0x9E3779B9
        self.aa = self.bb = self.cc = 0

        for x in range(4):
            a, b, c, d, e, f, g, h = self.mix(a, b, c, d, e, f, g, h)

        i = 0
        while i < 256:
            if flag:
                a = (a + self.randrsl[i]) % self.mod
                b = (b + self.randrsl[i + 1]) % self.mod
                c = (c + self.randrsl[i + 2]) % self.mod
                d = (d + self.randrsl[i + 3]) % self.mod
                e = (e + self.randrsl[i + 4]) % self.mod
                f = (f + self.randrsl[i + 5]) % self.mod
                g = (g + self.randrsl[i + 6]) % self.mod
                h = (h + self.randrsl[i + 7]) % self.mod

            a, b, c, d, e, f, g, h = self.mix(a, b, c, d, e, f, g, h)
            self.mm[i : i + 7 + 1] = a, b, c, d, e, f, g, h
            i += 8

        if flag:
            i = 0
            while i < 256:                    
                a = (a + self.mm[i]) % self.mod
                b = (b + self.mm[i + 1]) % self.mod
                c = (c + self.mm[i + 2]) % self.mod
                d = (d + self.mm[i + 3]) % self.mod
                e = (e + self.mm[i + 4]) % self.mod
                f = (f + self.mm[i + 5]) % self.mod
                g = (g + self.mm[i + 6]) % self.mod
                h = (h + self.mm[i + 7]) % self.mod

                a ^= 0xFFFFFFFF & b << 11
                d = (d + a) % self.mod
                b = (b + c) % self.mod
                
                b ^= 0x3FFFFFFF & (c >> 2)
                e = (e + b) % self.mod
                c = (c + d) % self.mod
                
                c ^= 0xFFFFFFFF & d << 8                
                f = (f + c) % self.mod
                d = (d + e) % self.mod
                d ^= e >> 16
                g = (g + d) % self.mod
                e = (e + f) % self.mod
                
                e ^= 0xFFFFFFFF & f << 10
                h = (h + e) % self.mod
                f = (f + g) % self.mod
                
                f ^= 0x0FFFFFFF & (g >> 4)
                a = (a + f) % self.mod
                
                g = (g + h) % self.mod
                g ^= 0xFFFFFFFF & h << 8
                b = (b + g) % self.mod
                h = (h + a) % self.mod
                
                h ^= 0x007FFFFF & (a >> 9)
                c = (c + h) % self.mod
                a = (a + b) % self.mod

                self.mm[i : i + 7 + 1] = a, b, c, d, e, f, g, h
                i += 8

        self.__isaac__()
        self.randcnt = 256