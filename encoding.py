import binascii

# Method from https://stackoverflow.com/questions/7396849/convert-binary-to-ascii-and-vice-versa

def int_32(number: int) -> int:
    return int(0xFFFFFFFF & number)

def to_bits(text, encoding='utf-8', errors='surrogatepass'):
    bits = bin(int(binascii.hexlify(text.encode(encoding, errors)), 16))[2:]
    return bits.zfill(8 * ((len(bits) + 7) // 8))

def from_bits(bits, encoding='utf-8', errors='surrogatepass'):
    n = int(bits, 2)
    return int2bytes(n).decode(encoding, errors)


def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))

def transform_str_to_int(text: str):
    return int.from_bytes(text.encode('utf-8'), 'little')


def transform_int_to_str(number: int):
    return number.to_bytes((number.bit_length() + 7) // 8, 'little').decode('utf-8')

# def newascii(ch):
#     ascii_value = ord(ch) + 100
#     return ascii_value


# def oldascii(ascii_val):
#     old_ascii = int(ascii_val) - 100
#     return old_ascii


# def transform_str_to_int(text: str):
#     string_ascii = ''

#     for i in text:
#         string_ascii += str(newascii(i))

#     return string_ascii


# def transform_int_to_str(newascii_string: int):
#     pack = ''
#     i = 0
#     dec_message = ''

#     while i < len(str(newascii_string)):

#         pack = newascii_string[i : i + 3]
#         dec_message += chr(oldascii(pack))
#         i = i + 3

#     return dec_message
