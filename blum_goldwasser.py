from math import log2, floor, ceil
from math_util import binary, fast_exp, extended_gcd, prime_number, fast_exp_teacher, is_probably_prime_miller_rabin, xor_str
from random import randint, randrange
from encoding import *
from binary_util import least_significant, least_significant_str

class BlumGoldwasser:
    def get_default_private_key(self) -> list:
        """
            Return a default private key.
        """

        return 65447, 34231

    def generate_private_key(self) -> list:       
        """
            Generate a private key.
        """

        while True:                        
            p = prime_number(140)
            
            if p % 4 == 3:
                break

        while True:                        
            q = prime_number(150)

            if q % 4 == 3:
                break
    
        return p, q   

    def get_public_key(self, private_key: list) -> int:
        """
            Return n, the public key.
        """
        return private_key[0] * private_key[1]

    def encrypt_list(self, private_key: list, plaintext: list) -> list:
        # Get the public key
        p, q = private_key
        n = self.get_public_key(private_key)
     
        # Block size in bits
        h = floor(log2(log2(n)))

        # The number of plaintext sub-blocks; each sub-block has length h
        t = ceil(len(plaintext) / h)        
            
        # Random number between 0 and n - 1.
        r = randrange(0, n)
        xi = (r * r) % n
        
        ciphertext = []

        for i in range(t):
            xi = pow(xi, 2, n)        
            pi = least_significant(xi, h)
            
            offset = i * h

            for j in range(h):
                if len(plaintext) == offset+j:
                    break
                
                ci = plaintext[offset + j] ^ pi[j]
                ciphertext.append(ci)
                
        xt = pow(xi, 2, n)
        return ciphertext, xt

    def decrypt_list(self, private_key: list, ciphertext: list, xt: int) -> list:
        # Get the public key
        p, q = private_key
        n = self.get_public_key(private_key)
    
        # Block size in bits
        h = floor(log2(log2(n)))

        # The number of plaintext sub-blocks; each sub-block has length h
        t = ceil(len(ciphertext) / h)

        d1 = pow((p+1) // 4, t+1, p-1)
        d2 = pow((q+1) // 4, t+1, q-1)

        _, a, b = extended_gcd(p, q)

        u  = pow(xt, d1, p)
        v  = pow(xt, d2, q)

        xi = (v * a * p + u * b * q) % n

        plaintext = []

        for i in range(t):
            xi = pow(xi, 2, n)
            pi = least_significant(xi, h)

            offset = i * h

            for j in range(h):
                if len(ciphertext) == offset+j:
                    break
                
                mi = ciphertext[offset + j] ^ pi[j]
                plaintext.append(mi)
        
        return plaintext

    def encrypt_str(self, private_key: list, plaintext: str) -> str:    
        plaintext = to_bits(plaintext)

        # Get the public key
        p, q = private_key
        n = self.get_public_key(private_key)
     
        # Block size in bits
        h = floor(log2(log2(n)))
    
        # The number of plaintext sub-blocks; each sub-block has length h
        t = ceil(len(plaintext) / h)    

        # Random number between 0 and n - 1.
        r = randrange(0, n)
        xi = (r * r) % n

        ciphertext = ''

        for i in range(t):     
            mi = plaintext[i * h:(i + 1) * h] # A sub-block
            xi = (xi * xi) % n                                    
            pi = least_significant_str(xi, h) # Take the least significant h bits of xi.                                            
            ci = xor_str(mi, pi)
            
            ciphertext += format(ci, '0' + str(h) + 'b')

        xt = (xi * xi) % n

        return ciphertext, xt
    
    def decrypt_str(self, private_key: list, ciphertext: str, xt: int) -> str:
        # Get the public key
        p, q = private_key
        n = self.get_public_key(private_key)
     
        # Block size in bits
        h = floor(log2(log2(n)))
        
        # The number of ciphertext sub-blocks.
        t = ceil(len(ciphertext) / h)
            
        dp = pow((p + 1) // 4,(t + 1) , (p - 1))
        dq = pow((q + 1) // 4,(t + 1) , (q - 1))

        up = pow(xt, dp, p)
        uq = pow(xt, dq, q)

        _, a, b = extended_gcd(p, q)        

        xi = (uq * a * p + up * b * q) % n
        plaintext = ''

        for i in range(t):
            ci = ciphertext[i * h:(i + 1) * h] # A sub-block
            xi = (xi * xi) % n
            pi = least_significant_str(xi, h) # Take the least significant h bits of xi.
            mi = xor_str(ci, pi)         
            plaintext += format(mi, '0' + str(h) + 'b')

        return from_bits(plaintext)