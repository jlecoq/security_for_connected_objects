from test_one_time_pad import *
from test_generator import *
from test_exponentiation import *
from test_primality import *
from test_key_exchange import *
from test_asymmetric_encryption import *

""" test one-time pad (symmetric encryption): """
# test_one_time_pad_without_prng()
# test_one_time_pad_with_linear_congruential_generator()

""" Test generator: """
# test_compare_generators_even_odd_number_count()
# test_compare_generators_speed()
# test_compare_generators_permutation_greater_smaller() 
# test_compare_generators_numbers_repetition()
# test_compare_execution_time_xorshift_linear_congruential_generator()
# test_compare_execution_time_mersenne_twister_lfsr()
# test_linear_congruential_generator_even_odd_number_frequency_comparison_with_different_parameters()

""" Test exponentiation: """
# test_fast_exponentiation_comparison()
# test_modular_exponentiation_comparison()

""" Test primality: """
# test_primality_fermat_miller_rabin_sympy()

""" Test key exchange: """
# test_diffie_hellman()

""" Test asymmetric encryption: """
# test_rsa()
# test_el_gamal()
# test_el_gamal_with_user_inputs()
# test_blum_goldwasser_with_list()
# test_blum_goldwasser_with_str()
