%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[a4paper,includeheadfoot,margin=2.54cm]{geometry}

\usepackage{float}
\usepackage{graphicx} % Required for the inclusion of images
\graphicspath{ {./images/} }

\usepackage{listings}
\usepackage{xcolor}
\usepackage{enumitem} % allow to custom letter or numbers for list

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    %numbers=left,                    
    %numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}

\urlstyle{same}

\setlength\parindent{0pt} % Removes all indentation from paragraphs

% \renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Security for the Internet of Things} % Title

\author{Julien \textsc{LECOQ}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Lab sessions date: & October, 2020 \\ % Date of the lab
Instructor: & Christophe \textsc{GUYEUX} % Instructor/supervisor
\end{tabular}
\end{center}

\begin{figure}[h]
\includegraphics[scale=0.073]{pyboard_2.jpg}
\centering
\end{figure}

\begin{abstract}
L'objectif de ces séances de master est de constituer une petite bibliothèque de sécurité pour le pyboard, compatible avec les ressources d'un tel objet, tout en intégrant les tenants et aboutissants de la sécurité dans l'internet des objets. A la fin des séances, on devra rendre ses codes python, ainsi qu'un manuscrit les commentant. 
\end{abstract}

\newpage

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Une courte introduction à la sécurité pour l'IoT}

Une introduction à la sécurité pour l'IoT, ainsi que les instructions de ce projet sont disponible à cette adresse :
\\\url{https://cours-info.iut-bm.univ-fcomte.fr/pmwiki-2.2.131/pmwiki.php/MonWiki/IotSecuriteIntroductionEn}

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Aperçu du code}

\subsection{Récupération}
Le code est disponible sur Gitlab à l'adresse : \\\url{https://blabla.fr}

\subsection{Architecture}

\subsection{Exécution}

L'intégralité du projet a été réalisé en Python 3.6.9, ainsi, pour exécuter le projet, il sera nécessaire de posséder un interpréteur de Python supportant cette version.
\\\\L'exécution de l'ensemble des fonctions se trouve au sein du fichier main.py. Les imports situé en haut de ce fichier contiennent l'ensemble de tous les tests pré-écrit afin de vérifier le travail demandé.
\\IMAGE IMPORTS
\\Il suffira simplement de décommenter les lignes de tests
\\IMAGE TESTS COMMENTE

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Un premier algorithme de chiffrement symétrique}

\subsection{Le masque jetable (One-time pad)}
Le chiffrement par la méthode du masque jetable consiste à combiner le message en clair avec une clé présentant les caractéristiques très particulières suivantes :

\begin{itemize}
    \item La clé doit être une suite de caractères au moins aussi longue que le message à chiffrer.
    \item Les caractères composant la clé doivent être choisis de façon totalement aléatoire.
    \item Chaque clé, ou « masque », ne doit être utilisée qu'une seule fois (d'où le nom de masque jetable).
\end{itemize}

Le message en clair, à chiffrer, se présente comme une suite de bits. La clé est une autre suite de bits, de même longueur.

On traite un à un les bits du message en clair, en effectuant un bitwise exclusive OR operation chacun avec le bit de même rang dans la clé.
\\Il s'agit-là de chiffrement symétrique, vu que l'on utilise le même masquage chez Alice et Bob : une même clé est utilisée pour chiffrer et déchiffrer, i.e. pour masquer et démasquer le message.

Le masque jetable est très efficace dans la mesure où le chiffrement/déchiffrement ne nécessite qu'une addition (modulo 2), donc linéaire. Ainsi, si les critères de Shannon sont respectés, cela fait de cet algorithme une bonne technique de chiffrement symétrique pour l'internet des objets. Le théorème de Shannon nous assure qu'il est alors impossible de retrouver le texte clair à partir du chiffré si on ne connaît pas la suite chiffrante : on dit que ce système est inconditionnellement sûr.
\\\\Voici les critères à respectés :

\begin{itemize}
    \item Le masque est réellement aléatoire (i.e., générateur cryptographiquement sûr),
    \item Il a la taille du message (on n'a pas copié-collé bout-à-bout un petit masque aléatoire, jusqu'à atteindre la taille du message).
    \item Il n'est utilisé qu'une fois (côté jetable du masque).
\end{itemize}

\subsection{Implémentation}
\label{one_time_pad_implementation:my}

Deux implémentations étaient demandées. Une implémentation avec une clef statique, crée en avance à la main puis une seconde implémentation avec une clef généré à l'aide d'un générateur de nombre aléatoire (un Linear Congruential Generator).
\\\\Puisque la première et la deuxième implémentation se ressemble, nous décrirons seulement la deuxième étant plus intéressante puisque possédant une génération de clef.
\\\\Le masque jetable étant un algorithme très simple, nous pouvons le décomposer facilement en quatre étapes : 

\begin{itemize}
    \item Création du message à envoyer.
    \item Création de la clef d'encodage et de décodage.
    \item Encodage du message.
    \item Décodage du message.
\end{itemize}

La création du message est très simple, elle se résume à la création d'une liste de bits (0, 1) correspondant aux informations du message.

\begin{lstlisting}[language=Python]
message = (0,0,1,0,0,1,0,1,0,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,0)
\end{lstlisting}

La seconde étape, la création de la clef, se fait de la manière suivante. Nous générons des nombres aléatoires que nous transformons sous forme binaire, puis de cette forme, nous extractons les bits que nous ajoutons à notre clef.

\begin{lstlisting}[language=Python]
def generate_key(self, message: list) -> list:
    """
        Generate and return a key of the same size than the message.
    """

    generator = LinearCongruentialGenerator()
    generated_key = []
    key_size = len(message)

    # While the generated key has not the same length as the key size
    while len(generated_key) < key_size:
        # We create a random integer.
        number = generator.rand_int()

        # We split into a binary list.
        binary_representation = binary(number)

        # For every bit, we add it to the key
        for bit in binary_representation:
            generated_key.append(bit)

            # If the generated key has the same length as the key size asked, 
            # we return the key.
            if len(generated_key) == key_size:
                return generated_key
\end{lstlisting}

Le masque (aussi appelé clef) est appliqué sur le message avec l'opérateur bit à bit XOR '\^{}'. Cela permet d'encoder le message.

\begin{lstlisting}[language=Python]
def encode(self, message: list, key: list) -> list:
    """
        Encode a message and return it.
    """        

    message_size = len(message)

    # Check if the message and the key have the same length.
    if message_size != len(key):
        print("The message and the key must have the same length.")
        return None

    ciphertext = []

    # Loop through the message and the key and perform a xor operation 
    # between every bit in order to encode the message.        
    for i in range(message_size):
        ciphertext.append(message[i] ^ key[i])

    return ciphertext
\end{lstlisting}

Nous pouvons maintenant décoder le message. Cependant, puisque l'encodage et le décodage s'effectue de la même manière, pour décoder, nous pouvons utiliser une référence de la fonction d'encodage.

\begin{lstlisting}[language=Python]
self.decode = self.encode
\end{lstlisting}

Enfin, un simple test nous permet de vérifier que les données encodées qui ont été décodé sont les mêmes que les données du message initial.

\begin{lstlisting}[language=Python]
def test_one_time_pad_with_linear_congruential_generator():
    """
        Run a simple test of the one-time pad encryption algorithm:
            - Create a message;
            - Generate a key;
            - Encode the message;
            - Decode the message;
            - Check if the decoded message is the same than the initial message.
    """

    # Start the test.
    print(f'{OKGREEN}RUN TEST: one-time pad with linear congruential generator:\n{ENDC}')

    one_time_pad = OneTimePad()

    # The initial message to transmit.
    message = (0,0,1,0,0,1,0,1,0,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,0)
    print(f'The message is: {message}')

    # The key use to encode and decode.    
    key = one_time_pad.generate_key(message)
    print(f'The key is: {key}')

    # Encode the message.
    encoded_message = one_time_pad.encode(message, key)
    print(f'The encoded message is: {encoded_message}')

    # Decode the message.
    decoded_message = one_time_pad.decode(encoded_message, key)
    print(f'The decoded message is: {decoded_message}\n')

    # Check if both message are equal.
    print(f'The message and the decode message are equal: {equal(message, decoded_message)}.\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')
\end{lstlisting}

\subsection{Résultats}

Après avoir exécuté ce test, nous obtenons le résultat suivant:

\begin{figure}[H]
\includegraphics[scale=0.6]{images/one-time_pad_execution_result.png}
\centering
\end{figure}    

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Constitution de générateur d'aléa}

Les générateurs de nombres aléatoires et pseudo-aléatoires sont une des bases de la sécurité informatique. Ils sont utilisés à flot afin de chiffrer et déchiffrer les messages à transmettre et transmis.

\subsection{Les générateurs crongruentiel linéaires}

\subsubsection{Présentation}

Nous présentons dans ce qui suit quelques générateurs réellement utilisés, en commençant par les générateurs congruentiels linéaires (LCG, 1948):

\begin{itemize}
    \item leur très faible complexité les rend utilisables dans l'internet des objets, même à très faible ressource;
    \item ils ne sont cependant pas sûr, aussi d'autres générateurs sont par la suite introduits.
\end{itemize}

Ces LCG sont définis ainsi : \(X_n+1 = (a.X_n+c) \mod m\), où a, c et m sont les paramètres du générateur.
\\\\Ces générateurs sont évidemment périodiques, et certains paramètres produisent de meilleures suites que d'autres, dans le sens où leurs périodes sont maximales. Pour obtenir de tels paramètres, il faut et il suffit que, si c \ne 0 :

\begin{itemize}
    \item c et m soient premiers entre eux (leur pgcd vaut 1).
    \item Pour chaque nombre premier p divisant m, (a−1) est un multiple de p.
    \item m multiple de 4 ⇒(a−1) multiple de 4.
\end{itemize}

\subsubsection{Travaux Pratiques}

\textbf{1. Programmer ce générateur.}
\\\\Le programme est disponible dans le fichier "linear\_congruential\_generator.py".
\\\\\textbf{2. Trouver de bons paramètres a, c, m.}
\\\\J'ai choisis de prendre les paramètres utilisés par la société Borland et disponible à cette adresse :
\\\url{https://en.wikipedia.org/wiki/Linear_congruential_generator}

\begin{itemize}
    \item a = 22 695 477
    \item c = 1
    \item m = \(2^{32}\)
\end{itemize}

Ces critères respectent les conditions afin de maximiser les "performances" du générateur. Ils permettent donc d'obtenir une période égale à m, pour toutes les valeurs de seed possibles.
\\\\\textbf{3. Vérifier que les nombres produits ressemblent à du hasard. Par exemple, regarder la fréquence d'apparition des nombres pairs et impairs.}
\\\\Ci-dessous, une capture d'écran d'un test présent dans le programme calculant la fréquence d'apparation des nombres pairs et impairs du LCG avec de mauvais paramètres (a, c, m) et avec de bon paramètres. Les tests ont été réalisé avec 1 000 000 de nombres :

\begin{figure}[H]
\includegraphics[scale=0.3]{images/number_of_occurences_even_odd_numbers_lcg.png}
\centering
\end{figure}

Comme nous pouvons le constater, le choix des paramètres est extrêmement important. Le LCG avec mauvais paramètres n'a produit que des nombres pairs, ce qui est très mauvais pour la génération de nombres aléatoires. Cela réduit le total de nombres produits possibles par 2, l'algorithme est donc moins productif et facilite en plus de cela la prédiction des nombres pouvant être généré par le générateur.
\\\\Le LCG avec bon paramètres, possède quant à lui, une très bonne distribution entre les nombres pairs et impairs (499 991 nombres pairs, 500 009 nombres impairs). C'est un des critères requis afin de classifier un PRNG comme cryptographiquement sûr, mais ce n'est pas le seul requis, nous en verrons d'autre part la suite.
\\\\\textbf{4. Intégrer cet outil de génération d'aléa au masque jetable précédemment programmé.}
\\\\Cet outil a été intégré au masque jetable (voir section \ref{one_time_pad_implementation:my})

\subsection{Le Xorshift}

\subsubsection{Présentation}

Ce générateur a été proposé il y a peu par Marsaglia, il possède à la base trois paramètres entiers a,b,c. On peut résumer son fonctionnement de la manière suivante :

\begin{enumerate}
    \item On part d'un vecteur de 32 bits, qui sert de graine.
    \item Pour calculer le nouveau terme à partir du terme actuel x :
    \item \begin{enumerate}
        \item On remplace x par le résultat du ou exclusif bit à bit entre x et sa version décalée de a bits vers la droite (décalage circulaire, les bits sortant à droite rentrent à gauche).
        \item On remplace x par le résultat du ou exclusif bit à bit entre x et sa version décalée de b bits vers la gauche (décalage circulaire).
        \item On remplace x par le résultat du ou exclusif bit à bit entre x et sa version décalée de c bits vers la droite (décalage circulaire).
    \end{enumerate}
    \item La valeur obtenue après ces trois opérations est le nouveau terme de la suite, à produire en sortie.
\end{enumerate}

Un bout de code en C détaillant le XORshift avec (12, 25, 27) comme paramètres serait donc :

\begin{center}
    \\\\x \^{}= x >> 12; // a
    \\x \^{}= x << 25; // b
    \\x \^{}= x >> 27; // c
\end{center}

\subsubsection{Travaux pratiques}

\textbf{1. Réaliser ce générateur.}
\\\\Le programme est disponible dans le fichier "xorshift32\_generator.py".
\\\\\textbf{2. Comparer le temps nécessaire à de la génération d'aléa suivant les deux techniques.}
\\\\Afin de comparer le temps d'exécution de ces deux algorithmes, j'ai décidé de générer 10 000 000 nombres. Ci-dessous, les résultats d'exécutions en secondes.

\begin{figure}[H]
\includegraphics[scale=0.3]{images/execution_time_comparison_lcg_xorshift32.png}
\centering
\end{figure}

On distingue très nettement la différence de rapidité d'exécution. Le LCG est 2,5 fois plus rapide que le Xorshift32.

\subsection{D'autres générateurs classiques}

Suivant votre avancée, vous pouvez compléter votre bibliothèque avec les générateurs suivants :
\\\\\textbf{1. Les Registres à décalage à rétroaction linéaire}
\\\\Un registre à décalage à rétroaction linéaire est un dispositif électronique ou logiciel qui produit une suite de bits qui peut être vue comme une suite récurrente linéare sur le crops fini F2 à 2 éléments (0 et 1).
Dans notre cas, c'est un dispositif logiciel (un algorithme Python) qui produit cette suite de bits.
\\\\L'idée est de commencer avec un registre rempli arbitrairemnt avec des 0 et des 1, puis de décaler cette suite d'un cran vers la droite. On remplira la position tout à gauche par la somme (XOR) entre plusieurs bits du registre (avant le décalage).
\\\\Exemple d'exécution d'un décalage: 

\begin{figure}[H]
\includegraphics[scale=0.5]{images/lfsr_shift_1.png}
\centering
\end{figure}

\begin{figure}[H]
\includegraphics[scale=0.5]{images/lfsr_shift_2.png}
\centering
\end{figure}

\begin{figure}[H]
\includegraphics[scale=0.5]{images/lfsr_shift_3.png}
\centering
\end{figure}

\begin{figure}[H]
\includegraphics[scale=0.5]{images/lfsr_shift_4.png}
\centering
\end{figure}

Son implémentation se trouve dans le fichier "linear\_feedback\_shift\_register.py".
\\\\\textbf{2. Le Mersenne-twister, générateur par défaut dans Python, R, Ruby, Maple, etc.}
\\\\C'est de loin le PRNG à usage général le plus largement utilisé. Son nom vient du fait que la durée de sa période est choisie pour être un prime de Mersenne.
L’algorithme est basé sur un TGSFR (twisted generalised shift feedback register, un type particulier de registre à décalage à rétroaction) et tient son nom d’un nombre premier de Mersenne. 
\\\\Il existe au moins deux variantes majeures, la plus répandue étant MT 19937, utilisant le nombre premier de Mersenne \(2^{19937} - 1\) et présente les propriétés suivantes :

\begin{itemize}
    \item sa période est de \(2^{19937} - 1\) ;
    \item il est uniformément distribué sur un grand nombre de dimensions (623 pour les nombres de 32 bits) ;
    \item il est aléatoire quel que soit le poids du bit considéré, suivant les tests Diehard, mais échoue systématiquement sur deux des tests BigCrush de TestU01.
\end{itemize}

Son implémentation se trouve dans le fichier "mersenne\_twister\_generator.py".
\\\\\textbf{3. Comparaison du temps d'exécution (Mersenne-Twister et LFSR).}
\\\\Comme précédemment, afin de comparer le temps d'exécution, je génère 10 000 000 nombres et je compare la différence de timestamp avant et après génération.

\begin{figure}[H]
\includegraphics[scale=0.3]{images/execution_time_comparison_lfsr_mersenne_twister.png}
\centering
\end{figure}

On s'aperçoit rapidement de la grande différence de rapidité, Mersenne-Twister est 2 fois plus rapide que LFSR. De plus, nous pouvons faire une autre remarque, même si Mersenne-Twister est nettement plus rapide que LFSR, il semble tout de même beaucoup plus lent que le LCG et un peu plus lent que le Xorshift32.
\\\\Use in IoT?

\subsection{Des générateurs cryptographiquement sûrs}
\\\\Les générateurs tels que le LCG sont rapides et nécessitent peu de ressources, on peut donc raisonnablement les utiliser dans le cadre de l'internet des objets, quand un besoin d'une source d'aléa se fait sentir. Cependant l'aléa engendré par de tels générateurs est d'une qualité assez mauvaise : ils ne passent par exemple pas des batteries de tests d'aléa tels que ceux présents dans la bibliothèque TestU01. De même, ils ne respectent pas la condition demandée par Shannon pour faire du masque jetable une technique cryptographiquement sûre.
\\\\On peut en effet formuler proprement, en termes mathématiques, le fait pour un générateur pseudo-aléatoire d'être cryptographiquement sûr. Sans rentrer dans les formules, cela signifie que si l'adversaire a n bits produits par un tel générateur, il ne peut pas, en temps raisonnable (polynomial) prédire le prochain bit sans, en moyenne, se tromper une fois sur 2. Il ne peut pas produire d'algorithme polynomial apte à compléter notre suite de bit, qui fasse moins d'erreurs qu'une simple prédiction aléatoire. A nouveau, tout cela se formalise via les théories des probabilités et de la complexité.

Il existe des générateurs cryptographiquement sûrs, tels que :
\begin{itemize}
    \item Blum Blum Shub
    \item ISAAC
\end{itemize}

\textbf{1. Le Blum Blum Shub}
\\\\Le générateur n'est pas approprié aux simulations, mais plutôt à la cryptographie, car il est assez lent.
\\\\On calcule la sortie de BBS en itérant la suite : \(x_{n+1} = (x_n)^2\ \mod M\) où "mod" est l'opérateur reste lors de la division par \(M = pq\), le produit de deux grands nombres premiers $p$ et $q$. La sortie de l'algorithme est le bit le moins significatif ou les derniers bits de \(x_{n+1}\).
\\\\Les deux nombres premiers, $p$ et $q$, devraient tous deux être congrus à 3 modulo 4 (cela garantit que chaque résidu quadratique possède une racine carrée qui soit également un résidu quadratique) et le PGCD de $\varphi(p-1)$ et $\varphi(q-1)$ doit être petit (ce qui fait que le cycle est long).
%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{How to efficiently calculate a power}

%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------

\section{Generation of prime numbers}



%----------------------------------------------------------------------------------------
%	SECTION 6
%----------------------------------------------------------------------------------------

\section{Key exchange}



%----------------------------------------------------------------------------------------
%	SECTION 7
%----------------------------------------------------------------------------------------

\section{Asymmetric encryption algorithms}



%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\bibliographystyle{apalike}

\bibliography{sample}

%----------------------------------------------------------------------------------------


\end{document}