from abc import ABCMeta, abstractmethod

class PseudoRandomNumberGenerator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def rand_int(self, mod=2 ** 32):        
        """
            Implemented in children classes.
        """
        pass

    def rand_ints(self, number_of_integers: int):
        values = []

        for i in range(number_of_integers):
            values.append(self.rand_int())

        return values

    # def rand(self):
        # """        
            # Return a float number in [0, 1).
        # """
# 
        # return self.rand_int() / (self.max + 1)
