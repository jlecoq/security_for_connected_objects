from collections import deque
from functools import reduce
from operator import xor
from pseudo_random_number_generator import PseudoRandomNumberGenerator 
from binary_util import binary_to_decimal
from math_util import binary, binary_of

class LinearFeedbackShiftRegister(PseudoRandomNumberGenerator):
    def __init__(self, seed=312993131, taps=[26, 28, 29]):
        self.state = binary_of(seed, 32)
        self.taps = taps

    def rand_int(self):
        ''' 
            Returns current state of the LFSR in integer form.
        '''

        t = self.state.pop()                               

        for tap in self.taps:
            t = self.state[tap] ^ t    

        self.state.insert(0, t)
    
        return binary_to_decimal(self.state)

