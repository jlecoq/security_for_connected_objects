from math_util import bezout, prime_number, fast_exp, fast_modpow, extended_gcd
from random import randint


class Rsa:
    def generate_pq(self):
        """
            Generate the pair (p, q).
        """        

        return prime_number(200), prime_number(215)


    def key(self, P1: int, P2: int) -> list:
        """
            Return the publick key (n, c) and the private key (d).
        """

        n = P1 * P2  # n is the encryption module
        phi = (P1 - 1) * (P2 - 1)
        c = randint(2, 15)

        r, d, v = bezout(c, phi)
        while r != 1 or d <= 2 or d >= phi:
            c = c + 1
            r, d, v = bezout(c, phi)

        return (n, c), d

    def decrypt(self, ciphertext: str, n: int, d: int, block_size=2) -> str:
        # Turns the string into a list of ints.
        list_blocks = ciphertext.split(' ')
        int_blocks = []

        for s in list_blocks:
            int_blocks.append(int(s))

        plaintext = ""

        # converts each int in the list to block_size number of characters
        # by default, each int represents two characters
        for i in range(len(int_blocks)):
            # decrypt all of the numbers by taking it to the power of d
            # and modding it by n n
            int_blocks[i] = fast_modpow(int_blocks[i], d, n)
            tmp = ""

            # take apart each block into its ASCII codes for each character
            # and store it in the message string
            for c in range(block_size):
                # print("ok5")
                tmp = chr(int_blocks[i] % 1000) + tmp
                int_blocks[i] //= 1000

            plaintext += tmp

        return plaintext

    def encrypt(self, plaintext: str, public_key: list, block_size=2) -> str:
        n, c = public_key

        encrypted_blocks = []
        ciphertext = -1

        if len(plaintext) > 0:
            # initialize ciphertext to the ASCII of the first character of message
            ciphertext = ord(plaintext[0])

        for i in range(1, len(plaintext)):
            # add ciphertext to the list if the max block size is reached
            # reset ciphertext so we can continue adding ASCII codes
            if (i % block_size == 0):
                encrypted_blocks.append(ciphertext)
                ciphertext = 0

            # multiply by 1000 to shift the digits over to the left by 3 places
            # because ASCII codes are a max of 3 digits in decimal
            ciphertext = ciphertext * 1000 + ord(plaintext[i])

        # Add the last block to the list
        encrypted_blocks.append(ciphertext)

        # Encrypt all of the numbers by taking it to the power of e
        # and modding it by n
        for i in range(len(encrypted_blocks)):
            encrypted_blocks[i] = str(fast_modpow(encrypted_blocks[i], c, n))

        # create a string from the numbers
        encrypted_message = " ".join(encrypted_blocks)

        return encrypted_message
