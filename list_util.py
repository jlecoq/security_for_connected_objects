def equal(list1: list, list2: list):
    list1_size = len(list1)

    if list1_size != len(list2):
        return False

    for i in range(list1_size):
        if list1[i] != list2[i]:
            return False

    return True