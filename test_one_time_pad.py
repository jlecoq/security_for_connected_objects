from one_time_pad import OneTimePad
from print_colors import OKGREEN, ENDC
from list_util import equal

def generate_key(key_size: int) -> list:
    """
        Generate and return the key of size "key_size"
    """

    generator = LinearCongruentialGenerator()
    generated_key = []

    # While the generated key has not the same length as the key size
    while len(generated_key) < key_size:
        # We create a random integer.
        number = generator.rand_int()

        # We split into a binary list.
        binary_representation = binary(number)

        # For every bit, we add it to the key
        for bit in binary_representation:
            generated_key.append(bit)

            # If the generated key has the same length as the key size asked, we return the key.
            if len(generated_key) == key_size:
                return generated_key

def encode(message: list, key: list) -> list:
    # Check if the message and the key have the same length.
    if len(message) != len(key):
        print("The message and the key must have the same length.")

    encoded_message = []

    # Loop through the message and the key and perform a xor operation between every bit 
    # in order to encode the message.
    for i in range(len(message)):
        encoded_message.append(message[i] ^ key[i])

    return encoded_message

decode = encode

def test_one_time_pad_without_prng():    
    # Start the test.
    print(f'{OKGREEN}RUN TEST: one-time pad without pseudo random number generator:\n{ENDC}')

    one_time_pad = OneTimePad()

    # The initial message to transmit.
    message = (0,0,1,0,0,1,0,1,0,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,0)
    print(f'The message is: {message}')

    # The key use to encode and decode.
    key = (1,0,1,1,0,0,0,1,0,1,1,1,0,0,1,0,0,0,1,1,1,0,1,0,0)
    print(f'The key is: {key}')

    # Encode the message.
    encoded_message = one_time_pad.encode(message, key)
    print(f'The encoded message is: {encoded_message}')

    # Decode the message.
    decoded_message = one_time_pad.decode(encoded_message, key)
    print(f'The decoded message is: {decoded_message}\n')

    # Check if both message are equal.
    print(f'The message and the decode message are equal: {equal(message, decoded_message)}.\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_one_time_pad_with_linear_congruential_generator():
    """
        Run a simple test of the one-time pad encryption algorithm:
            - Create a message;
            - Generate a key;
            - Encode the message;
            - Decode the message;
            - Check if the decoded message is the same than the initial message.
    """

    # Start the test.
    print(f'{OKGREEN}RUN TEST: one-time pad with linear congruential generator:\n{ENDC}')

    one_time_pad = OneTimePad()

    # The initial message to transmit.
    message = (0,0,1,0,0,1,0,1,0,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,0)
    print(f'The message is: {message}')

    # The key use to encode and decode.    
    key = one_time_pad.generate_key(message)
    print(f'The key is: {key}')

    # Encode the message.
    encoded_message = one_time_pad.encode(message, key)
    print(f'The encoded message is: {encoded_message}')

    # Decode the message.
    decoded_message = one_time_pad.decode(encoded_message, key)
    print(f'The decoded message is: {decoded_message}\n')

    # Check if both message are equal.
    print(f'The message and the decode message are equal: {equal(message, decoded_message)}.\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')