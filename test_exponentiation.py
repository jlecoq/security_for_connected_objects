from time import perf_counter
from math_util import fast_exp, fast_exp_teacher, fast_modpow, fast_recursive_modpow, round_half_up
from print_colors import OKGREEN, ENDC
from plt_util import display_bar_histogram

def test_fast_exponentiation_comparison():
    print(f'{OKGREEN}RUN TEST: Fast exponentiation:\n{ENDC}')

    base = 123563
    exp = 681321

    print('Parameters used:\n')
    print(f'base: {base}')
    print(f'exp: {exp}')

    elapsed_times = []

    initial_time = perf_counter()
    base ** exp
    final_time = perf_counter()
    elapsed_times.append(round_half_up(final_time - initial_time, 3))

    initial_time = perf_counter()
    pow(base, exp)
    final_time = perf_counter()
    elapsed_times.append(round_half_up(final_time - initial_time, 3))

    initial_time = perf_counter()
    fast_exp(base, exp)
    final_time = perf_counter()
    elapsed_times.append(round_half_up(final_time - initial_time, 3))

    initial_time = perf_counter()
    fast_exp_teacher(base, exp)
    final_time = perf_counter()
    elapsed_times.append(round_half_up(final_time - initial_time, 3))

    labels = ('base ** exp', 'pow(base, exp)', 'fast_exp(base, exp)', 'fast_exp_teacher(base, exp)')

    options = {
        'ylabel': 'Execution time (s)',
        'title': 'Execution time comparison of fast exponentiation methods.'
    }

    display_bar_histogram(elapsed_times, labels, options)

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_modular_exponentiation_comparison():
    print(f'{OKGREEN}RUN TEST: Modular exponentiation:\n{ENDC}')

    base = 123563947123
    exp = 681321
    mod = 121

    print('Parameters used:\n')
    print(f'base: {base}')
    print(f'exponent: {exp}')
    print(f'module: {mod}\n')

    elapsed_times = []

    initial_time = perf_counter()
    pow(base, exp) % mod
    final_time = perf_counter()
    result = round_half_up(final_time - initial_time, 3)
    elapsed_times.append(result)

    print(f'Elapsed time for the pow(base, exp) % mod: {result}.')

    initial_time = perf_counter()
    fast_exp_teacher(base, exp) % mod    
    final_time = perf_counter()
    result_fast_exp_and_mod = round_half_up(final_time - initial_time, 3)
    elapsed_times.append(result_fast_exp_and_mod)

    print(f'Elapsed time for the fast_exp_teacher(base, exp) % mod: {result_fast_exp_and_mod}.')

    initial_time = perf_counter()
    pow(base, exp, mod) 
    final_time = perf_counter()
    result = round_half_up(final_time - initial_time, 7)
    elapsed_times.append(result)

    print(f'Elapsed time for the pow(base, exp, mod): {result:.7f}.')

    initial_time = perf_counter()
    fast_recursive_modpow(base, exp, mod)
    final_time = perf_counter()
    result = round_half_up(final_time - initial_time, 7)
    elapsed_times.append(result)

    print(f'Elapsed time for the fast_recursive_modpow(base, exp, mod): {result:.7f}.')

    initial_time = perf_counter()
    fast_modpow(base, exp, mod)
    final_time = perf_counter()
    result_fast_exp_mod = round_half_up(final_time - initial_time, 7)
    elapsed_times.append(result_fast_exp_mod)
    
    print(f'Elapsed time for the fast_modpow(base, exp, mod): {result_fast_exp_mod:.7f}.\n')

    ratio = result_fast_exp_and_mod / result_fast_exp_mod

    print(f'The fast modular exponentiation is {ratio:.3f} times faster than the fast exponentiation and then applying the modulo.')

    labels = ('pow(base, exp) % mod', 'fast_exp_teacher(base, exp) % mod', 'pow(base, exp, mod)', 'fast_recursive_modpow(base, exp, mod)', 'fast_modpow(base, exp, mod)')

    options = {
        'ylabel': 'Execution time (s)',
        'title': 'Execution time comparison of modular exponentiation methods.'
    }

    display_bar_histogram(elapsed_times, labels, options)

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')