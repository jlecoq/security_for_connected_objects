from linear_congruential_generator import LinearCongruentialGenerator
from math_util import binary

class OneTimePad:

    def __init__(self):
        self.decode = self.encode

    def generate_key(self, message: list) -> list:
        """
            Generate and return a key of the same size than the message.
        """

        generator = LinearCongruentialGenerator()
        generated_key = []
        key_size = len(message)

        # While the generated key has not the same length as the key size
        while len(generated_key) < key_size:
            # We create a random integer.
            number = generator.rand_int()

            # We split into a binary list.
            binary_representation = binary(number)

            # For every bit, we add it to the key
            for bit in binary_representation:
                generated_key.append(bit)

                # If the generated key has the same length as the key size asked, 
                # we return the key.
                if len(generated_key) == key_size:
                    return generated_key

    def encode(self, message: list, key: list) -> list:
        """
            Encode a message and return it.
        """        

        message_size = len(message)

        # Check if the message and the key have the same length.
        if message_size != len(key):
            print("The message and the key must have the same length.")
            return None

        ciphertext = []

        # Loop through the message and the key and perform a xor operation 
        # between every bit in order to encode the message.        
        for i in range(message_size):
            ciphertext.append(message[i] ^ key[i])

        return ciphertext
