from random import randrange, randint
from isaac_generator import Isaac
from math import sqrt, log, floor

def is_prime(number):
    # Corner case
    if number <= 1:
        return False

    # Check from 2 to n-1
    for i in range(2, number):
        if number % i == 0:
            return False

    return True


def xor_str(num1: str, num2: str) -> int:
    return int(num1, 2) ^ int(num2, 2)


def binary(number: int) -> list:
    binary = []

    if number == 0:
        binary.append(0)
        return binary

    while number > 0:
        binary.insert(0, number % 2)
        number //= 2

    return binary

def binary_of(number: int, n_bits) -> list:
    binary_rep = binary(number)

    while len(binary_rep) < n_bits:
        binary_rep.insert(0, 0)

    return binary_rep

def gcd(a, b):
    """
        Performs the Euclidean algorithm and returns the gcd of a and b
    """

    # Everything divides 0
    if (b == 0):
        return a

    return gcd(b, a % b)


def extended_gcd(a, b):
    """
        Performs the Extended Euclidean algorithm to get the Bézout coefficients for two prime numbers (u, v).        
        Output : r = pgcd(a,b) et u, v entiers tels que a*u + b*v = r
    """

    old_t, t = 0, 1
    old_s, s = 1, 0

    while b != 0:
        quotient = a // b
        a, b = b, a - quotient * b
        old_t, t = t, old_t - quotient * t
        old_s, s = s, old_s - quotient * s

    return a, old_s, old_t


bezout = extended_gcd


def is_divisible(number1, number2):
    return True if number1 % number2 == 0 else False
"""
def prime_factors(n):
    p_factors = []

    # Print the number of two's that divide n
    while n % 2 == 0:
        p_factors.append(2)
        n = n / 2

    # n must be odd at this point
    # so a skip of 2 ( i = i + 2) can be used
    for i in range(3, int(sqrt(n)+1), 2):

        # while i divides n, print i ad divide n
        while n % i == 0:
            p_factors.append(i)
            n = n / i

    # Condition if n is a prime
    # number greater than 2
    if n > 2:
        p_factors.append(n)

    return p_factors
"""

def prime_factors(n):
    p_factors = set()

    # Print the number of two's that divide n
    while n % 2 == 0:
        p_factors.add(2)
        n = n // 2

    # n must be odd at this point
    # so a skip of 2 ( i = i + 2) can be used
    for i in range(3, int(sqrt(n)), 2):        
        # while i divides n, print i ad divide n
        while n % i == 0:
            p_factors.add(i)
            n = n // i
    
    # Condition if n is a prime
    # number greater than 2
    if n > 2:
        p_factors.add(n)

    return p_factors

def fast_exp(base, exp):
    res = 1

    while exp > 0:
        if exp % 2 == 1:
            res = res * base

        base = base * base
        exp = exp // 2

    return res

def fast_recursive_modpow(A, B, C): 
      
    # Base Cases 
    if (A == 0): 
        return 0
    if (B == 0): 
        return 1
      
    # If B is Even 
    y = 0
    if (B % 2 == 0): 
        y = fast_recursive_modpow(A, B / 2, C) 
        y = (y * y) % C 
      
    # If B is Odd 
    else: 
        y = A % C 
        y = (y * fast_recursive_modpow(A, B - 1,  
                             C) % C) % C 
    return ((y + C) % C) 

def fast_exp_teacher(base, exp):
    res = base
    first_one = True
    binary_val = binary(exp)

    for bit in binary_val:
        if bit == 1 and first_one:
            first_one = False
        elif bit == 1:
            res = res * res
            res = res * base
        else:
            res = res * res

    return res

# def prime_numbers2(n: int):
    # p_numbers = [x  for x in range(2, n)]
    # limit = n // 2
    # i = 0
    # j = 0
#
    # while j < len(p_numbers):
    # number = p_numbers[j]
#
    # while i < len(p_numbers):
    # if p_numbers[i] != number and is_divisible(p_numbers[i], number):
    # p_numbers.pop(i)
#
    # i += 1
    #
    # i = 0
    # j += 1
    #
    # return p_numbers


def prime_numbers(n: int):
    """
        Return the list of prime numbers between [2, n[.
    """

    # The list containing all the values between 0 and n-1.
    # We mark every values as 0 if they're not prime number.
    numbers = [0, 0] + [i for i in range(2, n)]
    limit = n // 2

    for i in range(limit):
        # It's a prime number
        if numbers[i] != 0:
            # étiqueter tous les multiples de i: de 2 * i à n (inclu) par pas de i

            # We delete (labelised by marking them as 0) every multiples of i: from 2 * i to n (excluded) with a step of i.
            # We do not remove them in memory because it's time consuming.
            for j in range(i * 2, n, i):
                numbers[j] = 0

    # We construct the list of prime numbers.
    # Every number different from 0 is a prime number.
    return [num for num in numbers if num != 0]

def simple_test_is_prime(number: int):
    """
        Return true if a number is prime, false otherwise.
        This is a very simple primality test and it does a lot of errors.
        Never use this function in production.
    """

    str_number = str(number)
    number_size = len(str_number)

    # If number has at least 2 digits.
    if number_size >= 2:
        # If its last digit is even, we say that he is not prime.
        if int(str_number[-1]) % 2 == 0:
            return False
        else:
            return True
    
    # if number is less than 2 digit, we say that he is prime.
    return True


def is_probably_prime_fermat(number: int):
    """
        Return true if a number is prime, false otherwise.
        It's a probabilistic primality test issued of the small theorem of Fermat, there is a little 
        probability that the result is incorrect.
    """
    return (2 << number - 2) % number == 1

def power(x, y, p):
    # Initialize result
    res = 1

    # Update x if it is more than or
    # equal to p
    x = x % p
    while (y > 0):

        # If y is odd, multiply
        # x with result
        if (y & 1):
            res = (res * x) % p

        # y must be even now
        y = y >> 1  # y = y/2
        x = (x * x) % p

    return res

# This function is called
# for all k trials. It returns
# false if n is composite and
# returns false if n is
# probably prime. d is an odd
# number such that d*2<sup>r</sup> = n-1
# for some r >= 1


def miller_test(d, n):
    # Pick a random number in [2..n-2]
    # Corner cases make sure that n > 4
    a = 2 + randint(1, n - 4)

    # Compute a^d % n
    x = power(a, d, n)

    if (x == 1 or x == n - 1):
        return True

    # Keep squaring x while one
    # of the following doesn't
    # happen
    # (i) d does not reach n-1
    # (ii) (x^2) % n is not 1
    # (iii) (x^2) % n is not n-1
    while (d != n - 1):
        x = (x * x) % n
        d *= 2

        if (x == 1):
            return False
        if (x == n - 1):
            return True

    # Return composite
    return False

# It returns false if n is
# composite and returns true if n
# is probably prime. k is an
# input parameter that determines
# accuracy level. Higher value of
# k indicates more accuracy.


def is_probably_prime_miller_rabin(n: int, k: int = 4):
    # Corner cases
    if (n <= 1 or n == 4):
        return False
    if (n <= 3):
        return True

    # Find r such that n =
    # 2^d * r + 1 for some r >= 1
    d = n - 1
    while (d % 2 == 0):
        d //= 2

    # Iterate given nber of 'k' times
    for i in range(k):
        if (miller_test(d, n) == False):
            return False
    
    return True

generator = Isaac()


def prime_number(number_of_digits: int):
    number = generator.rand_int_of(number_of_digits)

    # If the number is even, we add 1 to make it odd.
    if number % 2 == 0:
        number = number + 1

    is_probably_prime = False

    # While we didn't get a probably prime number, we add 2 to the current
    # number and check if it is a probably prime number. We do this until we get a probably
    # prime number.
    while not is_probably_prime:
        number += generator.rand_int_of(number_of_digits)
        # number += 2 We can also do this, to avoid getting an even number
        is_probably_prime = is_probably_prime_miller_rabin(number, 50)

    return number


def fast_modpow(base: int, exp: int, m: int) -> int:
    # Initialize result.
    result = 1

    # Update base if it is more than or equal to m.
    base = base % m

    if base == 0:
        return 0

    while exp > 0:
        # If exp is odd, multiply x with result.
        if (exp & 1) == 1:
            result = (result * base) % m

        # exp must be even now
        exp = exp >> 1  # exp = exp / 2
        base = (base * base) % m

    return result

def find_primitive_root_modulo_n(p_number: int):
    # Find value of Euler Totient function  
    # of n. Since n is a prime number, the  
    # value of Euler Totient function is n-1  
    # as there are n-1 relatively prime numbers. 
    phi = p_number - 1 

    # Find prime factors of phi.    
    p_factors = prime_factors(phi) # TODO: very slow if p_number > 15???

    # Check for every number from 2 to phi  
    for r in range(2, p_number):          
        # Iterate through all prime factors of phi.  
        # and check if we found a power with value 1  
        flag = False
        for it in p_factors:  
            # Check if r^((phi)/primefactors) 
            # mod n is 1 or not  
            if (pow(r, phi // it, p_number) == 1):            
                flag = True
                break
              
        # If there was no power with value 1.  
        if (flag == False):             
            return r  
  
    # If no primitive root found  
    return None


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return floor(n * multiplier + 0.5) / multiplier