from math_util import is_divisible, prime_numbers, miller_rabin, fast_exp, is_maybe_prime_fermat, is_probably_prime_miller_rabin, bezout, extended_gcd, prime_number
from test_primality import test_miller_rabin_test, test_fermat_test
from random import randint, randrange
from math import log2, floor, log, ceil