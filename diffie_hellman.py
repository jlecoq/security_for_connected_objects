from math_util import fast_exp, fast_modpow

def diffie_hellman():
    # Alice and Bob choose p and g.
    # p should be a prime number.
    # g should be a number strictly smaller than p.
    p = 23 
    g = 5
    print(f'Alice and Bob choose p = {p} and g = {g}.')

    # Alice choose a secret number.
    a = 6
    print(f'Alice chooses a secret number a = {a}.')

    # She sends A to Bob.
    A = fast_modpow(g, a, p)
    print(f'Alice computes A = {A} and send it to Bob.')

    # Bob choose a secret number.
    b = 15    
    print(f'Bob chooses a secret number b = {b}.')

    # He sends B to alice.    
    B = fast_modpow(g, b, p)
    print(f'Bob computes B = {B} and send it to Alice.')

    # Alice compute the secret key
    alice_secret_key = fast_modpow(B, a, p) 
    print(f'Alice computes the secret key = {alice_secret_key}.')
    
    # Bob compute the secret key
    bob_secret_key = fast_modpow(A, b, p)
    print(f'Bob computes the secret key = {bob_secret_key}.')
    
    # Alice secret key and Bob secret key are equals, in this case ("2")
    print(f'Both secret key are equal: {alice_secret_key == bob_secret_key}\n')