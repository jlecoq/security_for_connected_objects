from pseudo_random_number_generator import PseudoRandomNumberGenerator
from math_util import gcd, is_prime

class BlumBlumShub(PseudoRandomNumberGenerator):
    p = 30000000091
    q = 40000000003
    m = p * q
    seed = 737975716

    def set_parameters(self, seed, p, q):
        # Seed should be between 1 & m - 1
        self.seed = seed

        # Choose p and q such that gcd(p, q) = 1
        self.p = p
        self.q = q

        # Compute m, seed and m must be co-prime to M.
        self.m = p * q

    def rand_int(self): 
        self.seed = pow(self.seed, 2, self.m)
        return self.seed

    def are_good_parameters(self, seed, p, q):
        if seed == 0:
            return False
        elif seed == 1:
            return False
        elif not is_prime(p):
            return False
        elif not is_prime(q):
            return False

        m  = p * q

        # The seed must be co-prime to M
        if gcd(seed, m) != 1:
            return False

        return True
