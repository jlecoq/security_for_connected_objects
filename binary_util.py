def binary(number: int) -> list:
    binary = []

    if number == 0:
        binary.append(0)
        return binary

    while number > 0:
        binary.insert(0, number % 2)
        number //= 2

    return binary

def binary_to_decimal(n):
    num = n
    dec_value = 0
     
    # Initializing base 
    # value to 1, i.e 2 ^ 0
    base1 = 1
     
    len1 = len(num)

    for i in range(len1 - 1, -1, -1):
        if (num[i] == 1):
            dec_value += base1
        base1 = base1 * 2
     
    return dec_value

def least_significant_str(number: int, number_of_bits: int) -> str:
    """
        Return the least significant h bits of number.
    """

    return bin(number)[2:][-number_of_bits:] 

def least_significant(number: int, number_of_bits: int) -> list:
    """
        Return the least significant h bits of number.
    """

    return binary(number)[-number_of_bits:]
