from pseudo_random_number_generator import PseudoRandomNumberGenerator
from encoding import int_32

class Xorshift32(PseudoRandomNumberGenerator):
    def __init__(self):
        self.x = 2147483647 # 32 bit number: any nonzero start state will work.    

    def rand_int(self):
        """
            Return a random number of 32 bits.
        """
                
        self.x ^= int_32(self.x << 13)        
        self.x ^= int_32(self.x >> 17)        
        self.x ^= int_32(self.x << 5)

        return self.x