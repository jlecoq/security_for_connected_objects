import print_colors
from print_colors import OKGREEN, ENDC, FAIL, WARNING
from blum_goldwasser import BlumGoldwasser
from rsa import Rsa
from el_gamal import ElGamal
from math_util import prime_number

def test_blum_goldwasser_with_str():
    print(f'{OKGREEN}RUN TEST: Blum-Goldwasser using str:\n{ENDC}')
    blumGoldwasser = BlumGoldwasser()

    private_key = blumGoldwasser.get_default_private_key()

    print('Use default private key:')
    print('p:', private_key[0])
    print('q:', private_key[1], '\n')

    n = blumGoldwasser.get_public_key(private_key)
    print('Public key:')
    print('n:', n, '\n')

    # The initial message to transmit.
    message = "Hello world!"
    print("The initial message is: " + message)

    # Encrypt the message.
    cyphertext, xt = blumGoldwasser.encrypt_str(private_key, message)
    print("The encoded message is: " + cyphertext)

    # Decrypt the message.
    decoded_message = blumGoldwasser.decrypt_str(private_key, cyphertext, xt)
    print("The decoded message is: " + decoded_message + '\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')
        
def test_blum_goldwasser_with_list():
    print(f'{OKGREEN}RUN TEST: Blum-Goldwasser using binary list:\n{ENDC}')
    blumGoldwasser = BlumGoldwasser()

    private_key = blumGoldwasser.generate_private_key()

    print('Use generated private key:')
    print('p:', private_key[0])
    print('q:', private_key[1], '\n')

    n = blumGoldwasser.get_public_key(private_key)
    print('Public key:')
    print('n:', n, '\n')

    # The initial message to transmit.    
    message = [1,0,1,0,0,1]
    print("The message is:", message)

    # Encrypt the message.
    cyphertext, xt = blumGoldwasser.encrypt_list(private_key, message)
    print("The encoded message is:", cyphertext)

    # Decrypt the message.
    decoded_message = blumGoldwasser.decrypt_list(private_key, cyphertext, xt)
    print("The decoded message is:", decoded_message, '\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_rsa():
    print(f'{OKGREEN}RUN TEST: RSA\n{ENDC}')
    rsa = Rsa()

    # Generate two random probably prime numbers
    P1, P2 = rsa.generate_pq()
    
    public_key, private_key = rsa.key(P1, P2)

    # The initial message to transmit.        
    message = "Hello world!"
    print("The message is:", message)

    # Encrypt the message.
    cyphertext = rsa.encrypt(message, public_key)
    print("The encoded message is:", cyphertext)

    # Decrypt the message.
    decoded_message = rsa.decrypt(cyphertext, public_key[0], private_key)
    print("The decoded message is:", decoded_message, '\n')

    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_el_gamal():
    print(f'{OKGREEN}RUN TEST: el_gamal\n{ENDC}')

    el_gamal = ElGamal()
    p, g = el_gamal.generate_pg()

    public_key, private_key = el_gamal.key(p, g)

    # The initial message to transmit.        
    message = "Hello world!"
    print(f'The message is: {message}\n')

    # Encrypt the message.
    ciphertext = el_gamal.encrypt(message, public_key, private_key)
    print(f'The encoded message is: {ciphertext}\n')

    # Decrypt the message.
    decoded_message = el_gamal.decrypt(ciphertext, public_key, private_key)
    print(f'The decoded message is: {decoded_message}\n')
    
    # Test finished.
    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_el_gamal_with_user_inputs():
    print(f'{OKGREEN}RUN TEST: El Gamal with user inputs:\n{ENDC}')

    el_gamal = ElGamal()

    print(f'{WARNING}Do not generate a p of size > 16 to avoid some issues!{ENDC}')
    p_size = input("Enter the size in digit of p: ")
    print('\n')

    p, g = el_gamal.generate_pg_with_p_of_size(p_size)

    print(f'p: {p}')
    print(f'g: {g}\n')

    public_key, private_key = el_gamal.key(p, g)

    print(f'Public key: {public_key}')
    print(f'Private key: {private_key}\n')

    while True:
        print('What do you want to do now? Enter 1 to encode a message, 2 to decode a message or q to exit.')
        
        try: 
            response = input('Your response: ')

            if response == 'q':
                # Test finished.
                print(f'{OKGREEN}\nTEST FINISHED{ENDC}\n')
                return

            response = int(response)

        except ValueError: 
            print(f'{FAIL}You should enter a number between [1,2] or q.{ENDC}\n')            
            continue

        if response == 1:
            message = input("\nEnter the message you want to encrypt: ")
    
            ciphertext = el_gamal.encrypt(message, public_key, private_key)
            print(f'The encoded message is: {ciphertext}\n')

            decoded_message = el_gamal.decrypt(ciphertext, public_key, private_key)
            print(f'The decoded message is: {decoded_message}\n')

        elif response == 2:
            ciphertext = input("\nEnter the message to decrypt: ")
            ciphertext = el_gamal.transform_str_input_to_list(ciphertext)

            decoded_message = el_gamal.decrypt(ciphertext, public_key, private_key)
            print(f'The decoded message is: {decoded_message}\n')
        else:            
            print(f'{FAIL}You should enter a number between [1,2] or q.{ENDC}\n')
