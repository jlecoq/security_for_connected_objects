from encoding import int_32
from pseudo_random_number_generator import PseudoRandomNumberGenerator

class MersenneTwister(PseudoRandomNumberGenerator):
    def __init__(self, seed=5489):
        # MT19937
        self.n = 624
        self.state = [0] * self.n # Array to store the state of the generator.
        self.f = 1812433253
        self.m = 397
        self.u = 11
        self.s = 7
        self.b = 0x9D2C5680
        self.t = 15
        self.c = 0xEFC60000
        self.l = 18
        self.index = self.n + 1
        self.lower_mask = (1 << 31) - 1
        self.upper_mask = 1 << 31

        # Maximum integer number produced is 2**w - 1
        self.max = (1 << 32) - 1

        # Update state.
        self.state[0] = seed
        for i in range(1, self.n):
            self.state[i] = int_32(
                self.f * (self.state[i - 1] ^ (self.state[i - 1] >> 30)) + i)

    def twist(self):
        """ Generate the next n values from the series x_i"""
        for i in range(self.n):
            temp = int_32((self.state[i] & self.upper_mask) +
                          (self.state[(i + 1) % self.n] & self.lower_mask))
            temp_shift = temp >> 1

            if temp % 2 != 0:
                temp_shift = temp_shift ^ 0x9908b0df

            self.state[i] = self.state[(i + self.m) % self.n] ^ temp_shift

        self.index = 0

    def rand_int(self):
        if self.index >= self.n:
            self.twist()

        y = self.state[self.index]
        y = y ^ (y >> self.u)
        y = y ^ ((y << self.s) & self.b)
        y = y ^ ((y << self.t) & self.c)
        y = y ^ (y >> self.l)

        self.index += 1
        return int_32(y)

    def rand(self):
        """        
            Return a float number in [0, 1).
        """

        return self.rand_int() / 1 + self.max