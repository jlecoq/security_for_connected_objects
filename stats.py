from pseudo_random_number_generator import PseudoRandomNumberGenerator
from time import perf_counter
from math_util import round_half_up

def number_of_appearances_even_odd_numbers(numbers) -> list:
    """
        Return the number of appearances of even and odd numbers in a list.

        The number of appearances is stored in an array of the following form:

        [even, odd] => the index 0 contains the number of even numbers
                    => the index 1 contains the number of odd numbers.
    """
    odd_even_numbers = [0, 0]

    for i in range(len(numbers)):
        odd_even_numbers[numbers[i] % 2] += 1

    return odd_even_numbers

def generator_speed(generator: PseudoRandomNumberGenerator, number_of_integers: int) -> float: 
    initial_time = perf_counter()
    generator.rand_ints(number_of_integers)
    final_time = perf_counter()

    return round_half_up(final_time - initial_time, 3)

def generator_even_odd_count(generator: PseudoRandomNumberGenerator, number_of_integers: int) -> list:
    odd_even_count = [0, 0]

    for i in range(number_of_integers):
        odd_even_count[generator.rand_int() % 2] += 1
    
    return odd_even_count

def permutation_greater_smaller(generator: PseudoRandomNumberGenerator, number_of_iterations: int) -> list:
    """
        Compute the number of times a new number generated is greater or smaller than the previous number generated.

        permutation_list[0] => the numbers of number greater than the previous number generated before them.
        permutation_list[1] => the numbers of number smaller than the previous number generated before them.
        return permutation_list
    """

    permutation_list = [0] * 2
    
    while number_of_iterations > 0:
        if generator.rand_int() < generator.rand_int():
            permutation_list[0] += 1
        else:
            permutation_list[1] += 1

        number_of_iterations -= 1

    return permutation_list

def generator_min_max_and_average_repetition(generator: PseudoRandomNumberGenerator, number_of_integers: int) -> list:
    numbers = generator.rand_ints(number_of_integers)
    return min_max_and_average_repetition(numbers)

def min_max_and_average_repetition(numbers):
    """
        Return a dictionnary which own 3 properties:
        
        min_number_repetition, max_number_repetition, average_number_repetition
    """

    number_dict = {}
    
    min_number_repetition = 0
    max_number_repetition = 0 
    average_repetition = 0
    
    different_number_count = 0
    total_count = 0

    for i in numbers:
        total_count += 1

        if i not in number_dict:
            number_dict[i] = 1
            different_number_count += 1 
        else:
            number_dict[i] += 1

        if number_dict[i] > max_number_repetition: 
            max_number_repetition = number_dict[i]

    average_repetition = total_count / different_number_count

    first_iteration = True

    for number, count in number_dict.items():
        if first_iteration:
            min_number_repetition = count
            first_iteration = False
            break

        if count < min_number_repetition:
            min_number_repetition = count
        
    return (
        min_number_repetition, 
        max_number_repetition,
        round_half_up(average_repetition, 2)
    )

def bern(generator: PseudoRandomNumberGenerator, p):
    """ 
        Generate a Bernoulli Random Variable
        p: the probability of True
    """
    return generator.rand() <= p

def binomial(generator: PseudoRandomNumberGenerator):
        """
            Generate a Binomial Random Variable
            n: total times
            p: probability of success
        """

        a = [bern(generator) for n in range(n)]
        return a.count(True)