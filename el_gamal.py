from math_util import prime_factors, prime_number, fast_modpow, find_primitive_root_modulo_n, fast_exp
from encoding import transform_int_to_str, transform_str_to_int
from random import randint
from print_colors import WARNING, ENDC, OKGREEN, FAIL

class ElGamal:
    def generate_pg(self):
        p = prime_number(16)
        g = find_primitive_root_modulo_n(p) # Sometimes there is issues to find primitive root of high prime numbers. 

        return p, g

    def generate_pg_with_p_of_size(self, size: int):
        p = prime_number(16)
        g = find_primitive_root_modulo_n(p) # Sometimes there is issues to find primitive root of high prime numbers. 

        return p, g

    def key(self, p: int, g: int) -> list:
        a = randint(1, p - 1)
        A = fast_modpow(g, a, p)
        public_key = (p, g, A)
        private_key = a
        
        return public_key, private_key

    # Transform a str of char into a str of int.
    def transform_str_to_str_composed_of_int(self, msg):
        mBytes = msg.encode("utf-8")
        mInt = int.from_bytes(mBytes, byteorder="big")
        return str(mInt)

    # Encode the list of block with the public key.
    def encoderListBloc(self, public_key: list, listBloc: list):
        p, g, A = public_key

        # Pour chaque bloc
        for bloc in listBloc:
            k = randint(0, p - 1)

            y1 = pow(g, k, p)
            y2 = bloc * pow(A, k, p) % p
            newBloc = (y1, y2)
            
            listBloc[listBloc.index(bloc)] = newBloc

        return listBloc

    # Tranform the string composed of numbers into a list of block of 3 numbers.
    def decoupageBloc(self, msgConverti):
        listBloc = []

        # Pour chaque chiffre
        for numStr in msgConverti:
            # Initialisation au début
            if (len(listBloc) == 0):
                listBloc.append(numStr)
                continue # On skip à la prochaine itération

            index = len(listBloc) - 1;
            bloc = listBloc[index]

            # Si le bloc n'est pas complété
            if (len(bloc) < 3):
                listBloc[index] = bloc + numStr
            # Si le bloc est plein, on en crée un nouveau
            else:
                listBloc.append(numStr)
                
        for bloc in listBloc:
            listBloc[listBloc.index(bloc)] = int(bloc)
        
        return listBloc

    def encrypt(self, plaintext: str, public_key: list, private_key: int) -> list:        
        encoded_str_msg = self.transform_str_to_str_composed_of_int(plaintext) # String of ints
        encoded_list_msg = self.decoupageBloc(encoded_str_msg) # Lists of ints

        ciphertext_list = self.encoderListBloc(public_key, encoded_list_msg)

        return ciphertext_list

    # Converti une liste de bloc d'entier en chaîne de caractères
    def transform_list_of_int_to_str(self, listBloc):
        msgList = ""
        count = 0

        # Pour chaque bloc
        for bloc in listBloc:
            blocStr = str(bloc)

            # Si le bloc a moins de 3 chiffres
            if count < len(listBloc) - 1:
                # On rajoute 0 devant le nombre tant qu'il n'a pas 3 chiffres
                while len(blocStr) < 3:
                    blocStr = "0" + blocStr

            msgList += blocStr
            count += 1

        # On converti la chaîne de nombres en chaîne de caractère
        msgInt = int(msgList)
        mBytes = msgInt.to_bytes(((msgInt.bit_length() + 7) // 8), byteorder="big")
        m = mBytes.decode("utf-8")
        return m

    def transform_str_input_to_list(self, ciphertext: str):
        size_ciphertext = len(ciphertext)
        ciphertext = ciphertext[1 : size_ciphertext - 1]
        ciphertext = ciphertext.replace(')', '')    
        ciphertext = ciphertext.replace('(', '')
        ciphertext = ciphertext.replace(' ', '')

        count = 0
        str_to_append = ''
        block = []
        ciphertext_list = []

        for char in ciphertext:
            if char == ',':
                count += 1

                if count % 2 == 1:
                    block.append(int(str_to_append))
                    str_to_append = ''
                    continue

                if count % 2 == 0:
                    block.append(int(str_to_append))
                    str_to_append = ''
                    ciphertext_list.append(block)
                    block = []
                    continue
    
            str_to_append += char
    
        block.append(int(str_to_append))
        ciphertext_list.append(block)

        return ciphertext_list

    def decrypt(self, ciphertext, public_key: list, private_key: int) -> str:
        """
            Decrypt the list of block (the ciphertext)
        """

        p = public_key[0]
        listBloc = []

        # For each block.
        for bloc in ciphertext:
            y1 = bloc[0]
            y2 = bloc[1]

            puissance = p - 1 - private_key
            x = y2 * pow(y1, puissance, p) % p

            listBloc.append(x)

        return self.transform_list_of_int_to_str(listBloc)